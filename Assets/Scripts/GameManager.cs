﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }
    
    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            gameObject.name = "RootGameManager";
        }
    }

    public void LoadLevel(CampaignLevel level) {
        LoadScene(level.SceneName);
    }

    public void ReloadScene() {
        var scene = SceneManager.GetActiveScene();
        LoadSceneMethod(scene.name);
    }

    public void LoadMainMenu() {
        LoadScene("MainMenu");
    }
    
    public void LoadScene(string name) {
        LoadSceneMethod(name);
    }

    public static void LoadSceneMethod(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    private IEnumerator LoadSceneCoroutine(string name) {
        var sceneLoader = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        yield return sceneLoader;

        var curScene = SceneManager.GetActiveScene();
        var nextScene = SceneManager.GetSceneByName(name);
        
        if (nextScene.IsValid()) {
            SceneManager.SetActiveScene (nextScene);
            SceneManager.UnloadSceneAsync(curScene);
        }

    }
}
