﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.SceneManagement;

public class App : MonoBehaviour {
	private const string EnemyTag = "enemy";
	private const string ShellTag = "shell";
	
	[SerializeField] private int enemyCount = 0;
	public IntReactiveProperty enemyKilled = new IntReactiveProperty(0);
	
	public event Action allEnemyesKilled;
	public event Action<bool> gameOver;
	
	private void Awake () {
		var player = Player.Find();
		
		var enemyArr = GameObject.FindGameObjectsWithTag(EnemyTag);
		enemyCount = enemyArr.Length;
		
		foreach (var enemy in enemyArr)
			enemy.GetComponent<DamageSystem>().killed
				.Subscribe(x => enemyKilled.Value += 1);

		enemyKilled.Where(x => x == enemyCount)
			.Subscribe(OnAllEnemyesKilled);

//		var playerDamage = player?.GetComponent<DamageSystem>();

	}

	private void Start() {
		StartCoroutine(GameCoroutine());

//		foreach (var inputSystem in FindObjectsOfType<InputSystem>()) {
//			inputSystem.gameObject.SetActive(true);
//		}
	}

	private void OnAllEnemyesKilled(int i) {
		allEnemyesKilled?.Invoke();
		
		foreach (var inputSystem in FindObjectsOfType<InputSystem>()) {
			inputSystem.gameObject.SetActive(false);
		}
	}

	private IEnumerator GameCoroutine() {
		var wait = new WaitForSeconds(1f);
		
		while (true) {
			yield return wait;

			if (enemyKilled.Value == enemyCount) {
				yield return StartCoroutine(WaitShellFalled());
				yield break;
			}
			
			if (Player.Find() == null) {
				gameOver?.Invoke(false);
				yield break;
			}
		}
	}

	private IEnumerator WaitShellFalled() {
		var wait = new WaitForSeconds(1f);
		
		while (true) {
			yield return wait;

			var shellObjs = GameObject.FindGameObjectsWithTag(ShellTag);
			if (shellObjs.Length == 0) {
				gameOver?.Invoke(Player.Find() != null);
				yield break;
			}
		}
	}

	public void Restart() {
		GameManager.Instance.ReloadScene();
	}

	public void Exit() {
		GameManager.Instance.LoadMainMenu();
	}
	
	public static App FindApp() {
		var app = GameObject.FindGameObjectWithTag("app")?.GetComponent<App>();
		
		if (app == null)
			Debug.LogError("Cannon find app");

		return app;
	}
	
	public IEnumerable<Transform> FindEnemies() {
		var arr = GameObject.FindGameObjectsWithTag(EnemyTag);
		return arr.Select(x => x.GetComponent<Transform>());
	}
}
