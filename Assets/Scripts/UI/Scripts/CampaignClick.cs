﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CampaignClick : MonoBehaviour {
    public CampaignStatus campaignStatus;

    public UnityEventCampaign onActiveCampaign;
    public UnityEvent onNotActiveCampaign;
    
    public void Click() {
        CampaignProperties camp;
        
        if (campaignStatus.GetCurrentCampaign(out camp))
            onActiveCampaign.Invoke(camp);
        else
            onNotActiveCampaign.Invoke();
    }
}

[System.Serializable]
public class UnityEventCampaign : UnityEvent<CampaignProperties> { }