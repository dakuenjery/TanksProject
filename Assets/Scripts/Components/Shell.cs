﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QuickPool;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class Shell : MonoBehaviour {
	private Rigidbody rigidBody;
	private CapsuleCollider capsuleCollider;

	public GameObject explosionPrefab;

	public float explosionRaduis = 5f;
	public float damage = 30f;

	private static WaitForSeconds despawnWaitObject = new WaitForSeconds(2f);
	private static int framesBeforeActivate = 10;

	private void Awake() {
		rigidBody = GetComponent<Rigidbody>();
		capsuleCollider = GetComponent<CapsuleCollider>();
	}

	private void OnEnable() {
		rigidBody.velocity = Vector3.zero;
		rigidBody.rotation = Quaternion.identity;
		rigidBody.angularVelocity = Vector3.zero;
		capsuleCollider.enabled = false;
	}

	private void OnDisable() {

	}

	public void Fire(Vector3 force) {
		rigidBody.velocity = force;
		StartCoroutine(StartLifecycle());
	}

	private void OnCollisionEnter(Collision collision) {
		var exp = explosionPrefab.Spawn<Explosion>(transform.position, Quaternion.identity);
		exp.damage = damage;
		exp.radius = explosionRaduis;
		exp.Play();
		this.gameObject.Despawn();
	}

	private IEnumerator StartLifecycle() {
		int frame = 0;

		//while (++frame < framesBeforeActivate)
		yield return null;

		capsuleCollider.enabled = true;

		yield return despawnWaitObject;
		gameObject.Despawn();
	}
}
