﻿using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class CampaignLevel {
    public AvailableType status;
    
    [SerializeField] private string name;
    [SerializeField] private int rating;
    [SerializeField] private Image image;
    [SerializeField] private Object scene;

    public AvailableType Status => status;
    
    public string Name => name;
    public int Rating => rating;
    public Image Image => image;
    public string SceneName => scene.name;
}