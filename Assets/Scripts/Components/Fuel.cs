﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : MonoBehaviour {
    public int fuel = 100;
    
    private void OnTriggerEnter(Collider other) {
        var moveSystem = other.gameObject.GetComponent<MoveSystem>();

        if (moveSystem != null) {
            moveSystem.fuelCount.Value = fuel;
            gameObject.SetActive(false);
        } else {
            Debug.LogError($"MoveSystem not found. GameObject name: {other.gameObject}");
        }
    }
}
