﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameUI : MonoBehaviour {

	public UIController joystick;
	public UIController playerInfo;

	public UIController gameOverPanel;
	public UIController winPanel;
	
	void Start () {
		var player = Player.Find();
		var app = App.FindApp();
		
		joystick.Show();
		playerInfo.Show();

		if (app != null) {
			app.gameOver += win => {
				if (win)
					winPanel.Show();
				else
					gameOverPanel.Show();
			};
		} 
	}
}
