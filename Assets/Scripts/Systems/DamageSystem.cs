﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QuickPool;
using UniRx;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class DamageSystem : MonoBehaviour {

	public GameObject explosionPrefab;

	public bool deathable = true;
	public IntReactiveProperty hp = new IntReactiveProperty(3);
	public Subject<Unit> killed = new Subject<Unit>();

	private float lastDamage = 0;

	private void Awake() {
		hp.Where(x => x <= 0).Subscribe(Kill);
	}

	public void AddExplosion(Vector3 pos, float damage, float radius) {
		var distance = Vector3.Distance(transform.position, pos);

		if (distance < radius) {
			radius = (radius - distance) / radius;
			lastDamage = damage * radius;

			hp.Value -= (int)lastDamage;
		}
	}

	private void Kill(int q) {
		Debug.Log("KILL!");

		if (deathable) {
//			this.gameObject.Despawn();
			gameObject.SetActive(false);
			
			explosionPrefab.Spawn<Explosion>(transform.position, Quaternion.identity)
				.Play();
			
			killed.OnNext(Unit.Default);
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos() {
		Handles.Label(transform.position, string.Format("♥ {0}\n☢ {1}", hp, lastDamage));
	}
#endif
}
