﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {
    public int ammo = 100;
    
    private void OnTriggerEnter(Collider other) {
        var cannonSystem = other.gameObject.GetComponent<CannonSystem>();

        if (cannonSystem != null) {
            cannonSystem.ammoCount.Value = ammo;
            this.gameObject.SetActive(false);
        } else {
            Debug.LogError($"CannonSystem not found. GameObject name: {other.gameObject}");
        }
    }
}
