﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UIController : MonoBehaviour {
    
    public float duration = 1f;
    public AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    public void Show() {
        transform.DOScale(0, duration)
            .From()
            .SetEase(curve)
            .OnStart(() => gameObject.SetActive(true));
    }

    public void Hide() {
        transform.DOScale(0, duration)
            .SetEase(curve)
            .OnComplete(() => gameObject.SetActive(false));
    }
}
