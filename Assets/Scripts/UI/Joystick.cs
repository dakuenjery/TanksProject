﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IDragHandler, IEndDragHandler {

	[SerializeField] private RectTransform joystick;
	[SerializeField] private RectTransform centerRect;

	[SerializeField] private InputSystem input;

	public MoveSystem moveSystem;

	private Rect rect;
	
	private float fieldRadius = 10f;

	private void Start() {
		rect = GetComponent<RectTransform>().rect;
		fieldRadius = rect.width / 2;
		
#if UNITY_EDITOR
		if (rect.width != rect.height)
			Debug.LogError("Joystick width != height");
#endif
	}

	public void OnDrag(PointerEventData eventData) {
		if (!input.enabled)
			return;
		
		var vec = eventData.position - (Vector2)centerRect.position;
		var vecNormal = vec.normalized;
		var pos = vecNormal * Mathf.Clamp(vec.magnitude * 2, 0f, fieldRadius);
		joystick.anchoredPosition = pos;
		
		moveSystem.direction = vecNormal;
	}

	public void OnEndDrag(PointerEventData eventData) {
		joystick.anchoredPosition = Vector2.zero;

		moveSystem.direction = Vector2.zero;
	}
}
