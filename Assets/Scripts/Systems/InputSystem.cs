﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour {

	[SerializeField] private int type;
	[SerializeField] private LayerMask mouseHitLayerMask;
	
	private float downTime = 0f, upTime = 0f;
	private Vector3 downPos, curPos;
	
	public void Init(int type) {
		this.type = type;
	}
	
	public bool ClickInWorld(ref Vector3 vec) {
		if (upTime > 0f) {
			if (Vector3.Distance(downPos, curPos) < 1f) {
				vec = MouseScreenToWorld(curPos);
				return true;
			}
		}
		
		return false;
	}
	
	public bool MouseUnholdInWorld(ref Vector3 vec) {
		if (upTime > 0f) {
			vec = MouseScreenToWorld(curPos);
			return true;
		}
		
		return false;
	}

	public bool MouseHoldInWorld(ref Vector3 vec) {
		if (downTime > 0f) {
			vec = MouseScreenToWorld(curPos);
			return true;
		}
		
		return false;
	}

	public bool Click(ref Vector2 vec) {
		if (upTime > 0f) {
			if (Vector3.Distance(downPos, curPos) < 1f) {
				vec = MouseScreenToWorld(curPos);
				return true;
			}
		}
		
		return false;
	}

	public bool MouseHold(ref Vector2 vec) {
		if (downTime > 0f) {
			vec = downPos;
			return true;
		}
		
		return false;
	}

	public bool MouseUnhold(ref Vector2 vec) {
		if (upTime > 0f) {
			vec = curPos;
			return true;
		}

		return false;
	}

	private void Start() {
		Debug.Log($"Touch supported: {Input.touchSupported}");
	}
	
	private void Update() {
		if (Input.GetMouseButtonDown(0)) {
			downTime = Time.unscaledTime;
			downPos = curPos = Input.mousePosition;
		} else if (Input.GetMouseButtonUp(0)) {
			upTime = Time.unscaledTime;
			curPos = Input.mousePosition;
		} else if (Input.GetMouseButton(0)) {
			curPos = Input.mousePosition;
		} else { // hold out
			downTime = upTime = 0f;
		}
	}

	private void OnDisable() {
		upTime = 0;
	}

	private Vector3 MouseScreenToWorld(Vector3 pos) {
		var camera = Camera.main;
		RaycastHit hit;
		Physics.Raycast(camera.ScreenPointToRay(pos), out hit, float.MaxValue, mouseHitLayerMask);
		return hit.point;
	}
}
