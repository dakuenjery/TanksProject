﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CampaignLevels : MonoBehaviour {

	public CampaignProperties campaign;

	public Transform content;
	
	public CampaignLevelSelectedEvent availableLevelSelected;
	public CampaignLevelSelectedEvent lockedLevelSelected;

	private UICollectionHandler<CampaignLevel> collectionHandler;

	public void SetCampaign(CampaignProperties campaign) {
		this.campaign = campaign;
		collectionHandler.Init(campaign.Levels);
	}
	
	void Awake () {
		collectionHandler = new UICollectionHandler<CampaignLevel>(content, Bind);
	}

	private void Bind(Transform item, CampaignLevel level) {
		var title = item.GetChild(0).GetComponent<Text>();

		title.text = level.Name;
		item.gameObject.SetActive(true);

		item.GetComponent<Button>().onClick
			.AddListener(() => OnLevelSelected(level));
	}

	private void OnLevelSelected(CampaignLevel camp) {
		switch (camp.Status) {
			case AvailableType.NotAvailable:
				lockedLevelSelected.Invoke(camp);
				break;
			case AvailableType.Available:
				availableLevelSelected.Invoke(camp);
				break;
			case AvailableType.Passed:
				Debug.Log($"OnLevelSelected {camp.Name}: AvailableType.Passed");
				break;
		}
	}
}


[System.Serializable]
public class CampaignLevelSelectedEvent : UnityEvent<CampaignLevel> { }