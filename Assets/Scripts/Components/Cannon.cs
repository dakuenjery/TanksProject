﻿using System.Collections;
using System.Collections.Generic;
using QuickPool;
using UnityEngine;

public class Cannon : MonoBehaviour {
    private AudioSource audio;

    public GameObject ShellPrefab;
    public Transform ShellSpawnPoint;

    public int BulletsForShot = 1;

    public float FireForce = 10f;

    private void Awake() {
        audio = GetComponent<AudioSource>();
    }

    public void RotateCannon(Vector3 dir) {
        var q = Quaternion.LookRotation(dir).eulerAngles.y;
        transform.rotation = Quaternion.Euler(0, q + 90, 0);
    }
    
    public void Shot(Vector3 dir) {
        RotateCannon(dir);
        
        var rot = ShellSpawnPoint.rotation;
        var shell = ShellPrefab.Spawn<Shell>(ShellSpawnPoint.position, rot);
        shell.Fire(ShellSpawnPoint.forward * dir.magnitude * FireForce);
        
        audio.Play();
    }
}
