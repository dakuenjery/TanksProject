﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class FuelUIComponent : MonoBehaviour {

	[SerializeField] private LinearChildAnimator childAnimator;

	private void Awake() {
		childAnimator.parent = transform;
		
		var player = Player.Find();
		
		player?.GetComponent<MoveSystem>().fuelCount
			.StartWith(0)
			.Pairwise()
			.Subscribe(x => childAnimator.Update(Mathf.CeilToInt(x.Current), Mathf.CeilToInt(x.Previous)));
	}
}
