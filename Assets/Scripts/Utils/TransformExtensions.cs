﻿using UnityEngine;

namespace Utils {
    public static class TransformExtensions {
        public static T GetComponentInChildren<T>(this Transform transform, string childname)
            where T: Component
        {
            var child = transform.Find(childname);

            if (child == null) {
                Debug.LogError($"Cannon find child '{childname}' in '{transform.name}'");
                return null;
            }

            var comp = child.GetComponent<T>();
            
            if (comp == null)
                Debug.LogError($"Cannon find component '{typeof(T)}' in {child.name}");

            return comp;
        }
    }
}