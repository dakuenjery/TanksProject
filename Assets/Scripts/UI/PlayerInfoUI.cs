﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using QuickPool;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using System.Linq;

public class PlayerInfoUI : MonoBehaviour {
	private const string HideTag = "hide";
	private const string ShowTag = "show";
	
	
	[SerializeField] private Transform hp;
	[SerializeField] private Transform fuel;
	[SerializeField] private Transform ammo;

	[SerializeField] private float tweenTime = .2f;
	[SerializeField] private float tweenDelayTime = .12f;

	private void Awake() {
		var player = Player.Find();

		var damageSystem = player?.GetComponent<DamageSystem>();
		var moveSystem = player?.GetComponent<MoveSystem>();
		var cannonSystem = player?.GetComponent<CannonSystem>();
		
//		damageSystem?.hp
//			.StartWith(0)
//			.Pairwise()
//			.Subscribe(x => UpdateSequence(hp, x.Current, x.Previous));
		
		moveSystem?.fuelCount
			.StartWith(0)
			.Pairwise()
			.Subscribe(x => UpdateSequence(fuel, Mathf.CeilToInt(x.Current), Mathf.CeilToInt(x.Previous)));

		cannonSystem?.ammoCount
			.StartWith(0)
			.Pairwise()
			.Subscribe(x => UpdateSequence(ammo, x.Current, x.Previous));
	}

	private void UpdateSequence(Transform obj, int current, int previous) {
		var x = current - previous;
		
		if (x > 0) {
			// instantiate childred if needed
			for (int count = current - obj.childCount; count >= 0; --count)
				CloneChild(obj.GetChild(0), HideTag);

			ShowChildren(obj, x);
		}
		
		if (x < 0) {
			HideChildren(obj, -x);
		}
	}

	private void ShowChildren(Transform obj, int count) {
		int activated = 0;

		for (int i = obj.childCount - 1; i > 0; --i) {
			var child = obj.GetChild(i);
			
			if (!child.gameObject.activeSelf && !child.CompareTag(ShowTag)) {
				child.gameObject.SetActive(true);
				child.tag = ShowTag;
				child.transform.localScale = Vector3.one;
				child.DOScale(0, tweenTime).From()
					.SetDelay(tweenDelayTime * activated++);
				
				if (activated >= count)
					break;
			}
		}
	}

	private void HideChildren(Transform obj, int count) {
		int activated = 0;
		
		foreach (Transform child in obj) {
			if (!child.CompareTag(HideTag)) {
				child.tag = HideTag;
				child.DOScale(0, tweenTime)
					.SetDelay(tweenDelayTime * activated++)
					.OnComplete(() => child.gameObject.SetActive(false));
				
				if (activated >= count)
					break;
			}
		}
	}

	private Transform CloneChild(Transform obj, string tag) {
		var child = Instantiate(obj).GetComponent<Transform>();
		child.SetParent(obj.parent);
		child.SetAsFirstSibling();
		child.localScale = Vector3.one;
		child.gameObject.SetActive(false);
		child.tag = tag;
		return child;
	}
}

[System.Serializable]
public class LinearChildAnimator {
	private const string HideTag = "hide";
	private const string ShowTag = "show";
	
	public Transform parent;
	
	[SerializeField] private float tweenTime = .2f;
	[SerializeField] private float tweenDelayTime = .12f;

	public LinearChildAnimator(Transform parent) {
		this.parent = parent;
	}
	
	public void Update(int current, int previous) {
		var x = current - previous;
		
		if (x > 0) {
			// instantiate childred if needed
			for (int count = current - parent.childCount; count >= 0; --count)
				CloneChild(parent.GetChild(0), HideTag);

			ShowChildren(parent, x);
		}
		
		if (x < 0) {
			HideChildren(parent, -x);
		}
	}

	private void ShowChildren(Transform obj, int count) {
		int activated = 0;

		for (int i = obj.childCount - 1; i > 0; --i) {
			var child = obj.GetChild(i);
			
			if (!child.gameObject.activeSelf && !child.CompareTag(ShowTag)) {
				child.gameObject.SetActive(true);
				child.tag = ShowTag;
				child.transform.localScale = Vector3.one;
				child.DOScale(0, tweenTime).From()
					.SetDelay(tweenDelayTime * activated++);
				
				if (activated >= count)
					break;
			}
		}
	}

	private void HideChildren(Transform obj, int count) {
		int activated = 0;
		
		foreach (Transform child in obj) {
			if (!child.CompareTag(HideTag)) {
				child.tag = HideTag;
				child.DOScale(0, tweenTime)
					.SetDelay(tweenDelayTime * activated++)
					.OnComplete(() => child.gameObject.SetActive(false));
				
				if (activated >= count)
					break;
			}
		}
	}

	private Transform CloneChild(Transform obj, string tag) {
		var child = Object.Instantiate(obj).GetComponent<Transform>();
		child.SetParent(obj.parent);
		child.SetAsFirstSibling();
		child.localScale = Vector3.one;
		child.gameObject.SetActive(false);
		child.tag = tag;
		return child;
	}
}