﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;

[CustomEditor(typeof(MoveSystem))]
public class PlayerMoveSystemEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var obj = serializedObject.targetObject;
		
        var message = obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(prop => Attribute.IsDefined((MemberInfo) prop, typeof(HelpBoxInfoAttribute)))
            .Select(field => $"{field.Name}: {field.GetValue(obj).ToString()}")
            .Aggregate("", (acc, val) => acc.Length > 0 ? $"{acc}\n{val}" : val);
		
        EditorGUILayout.HelpBox(message, MessageType.None);
    }
	
}