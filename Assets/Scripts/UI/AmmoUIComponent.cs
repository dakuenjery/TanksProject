﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUIComponent : MonoBehaviour {

	private Text ammoCount;

	private void Awake() {
		this.ammoCount = transform.GetChild(0).GetComponent<Text>();
		var player = Player.Find();
		
		player?.GetComponent<CannonSystem>().ammoCount
			.StartWith(0)
			.Subscribe(x => ammoCount.text = $"{x}");
	}
}
