﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UICollectionHandler<T> {
    private Transform content;
    private Action<Transform, T> bind;

    public UICollectionHandler(Transform content, Action<Transform, T> bind) {
        this.content = content;
        this.bind = bind;
    }

    public void Init(IList<T> elements) {
        var instantiateCount = elements.Count - content.childCount;
        for (int i = 0; i < instantiateCount; ++i) {
            var item = GameObject.Instantiate(content.GetChild(0));
            item.SetParent(content);
            item.localScale = Vector3.one;
            item.localEulerAngles = Vector3.zero;
            item.localPosition = Vector3.zero;
            item.gameObject.SetActive(false);
        }

        int childId = 0;
        for (; childId < elements.Count; ++childId) {
            bind.Invoke(content.GetChild(childId), elements[childId]);
        }
		
        for (; childId < content.childCount; ++childId) {
            content.GetChild(childId).gameObject.SetActive(false);
        }
    }
}