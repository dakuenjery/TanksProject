﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class CameraScript : MonoBehaviour {
	public Rigidbody player;

	public float FollowSpeed = .5f;
	public float ForwardFactor = .17f;
	public float UpFactor = 1.1f;

	public float EnemiesFollowSpeed = .5f;

	public EnemyBaiter enemyBaiter;

	private Vector3 camOffset;
	private int triggerCount = 0;

	private void OnEnemyTriggerExit(Collider collider) {
		if (--triggerCount <= 0) {
			triggerCount = 0;
			camOffset = Vector3.zero;
		}
	}

	private void OnEnemyTriggerEnter(Collider collider) {
		triggerCount += 1;
		var enemyPos = collider.transform.position;
	}

	private void LateUpdate() {
		var currentPos = transform.position;
		
		if (!player.gameObject.activeSelf)
			return;
		
		var playerPosition = player.position;
		var playerVelocity = player.velocity;
		
		var f = playerVelocity.magnitude * ForwardFactor;

		playerVelocity.y += f * UpFactor;

		var dest = playerPosition + playerVelocity * f;
		currentPos = Vector3.Lerp(currentPos, dest + enemyBaiter.camOffset, Time.smoothDeltaTime * FollowSpeed);

		transform.position = currentPos;
	}
}
