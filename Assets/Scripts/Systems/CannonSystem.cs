﻿using QuickPool;
using UnityEngine;
using UniRx;
using Utils;

public class CannonSystem : MonoBehaviour {
    private static string CHILD_CANNON = "Body/Cannon";
    
    private Cannon cannon;
    private float lastShotTime = 0f;

    private Vector3 mouseDownPos;
    private LayerMask terrainLayerMask;

    public float shotRate = .2f;
    public IntReactiveProperty ammoCount = new IntReactiveProperty(10);
    public BoolReactiveProperty unlimitedAmmo = new BoolReactiveProperty(false);

    [SerializeField] private InputSystem input;

    private void Awake() {
        terrainLayerMask = LayerMask.GetMask("terrain");
        cannon = transform.GetComponentInChildren<Cannon>(CHILD_CANNON);
    }

    private void Update() {
        var vec = Vector3.zero;

        if (input.ClickInWorld(ref vec))
            Shot(vec);
    }

    public void Shot(Vector3 position) {
        if (cannon == null || (!unlimitedAmmo.Value && ammoCount.Value < cannon.BulletsForShot))
            return;
        
        var time = Time.time;
        if (time - lastShotTime <= shotRate)
            return;

        if (unlimitedAmmo.Value == false)
            ammoCount.Value -= cannon.BulletsForShot;
        
        cannon.Shot(position - transform.position);

        lastShotTime = time;
    }

    private Vector3 MouseScreenToWorld(Vector3 pos) {
        var camera = Camera.main;
        RaycastHit hit;
        Physics.Raycast(camera.ScreenPointToRay(pos), out hit, float.MaxValue, terrainLayerMask);
        return hit.point;
    }
}