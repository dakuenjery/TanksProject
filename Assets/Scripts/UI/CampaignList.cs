﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CampaignList : MonoBehaviour {

	public CampaignStatus campaign;

	public Transform content;
	
	public CampaignSelectedEvent availableCampaignSelected;
	public CampaignSelectedEvent lockedCampaignSelected;
	
	void Start () {
		campaign.UpdateCampaigns();
		
		foreach (var camp in campaign.Campaigns)
			CreateCampaignItem(camp);
		
		content.GetChild(0).gameObject.SetActive(false);
	}

	private void CreateCampaignItem(CampaignProperties camp) {
		var item = Instantiate(content.GetChild(0));

		var title = item.GetChild(0).GetComponent<Text>();

		title.text = camp.Title;
		
		item.SetParent(content);
		item.SetAsLastSibling();
		item.localScale = Vector3.one;
		item.localEulerAngles = Vector3.zero;
		item.localPosition = Vector3.zero;
		item.gameObject.SetActive(true);

		item.GetComponent<Button>().onClick
			.AddListener(() => OnCampaignSelected(camp));
	}
	
	private void Bind(Transform item, CampaignProperties camp) {
		var title = item.GetChild(0).GetComponent<Text>();

		title.text = camp.Title;
		item.gameObject.SetActive(true);

		item.GetComponent<Button>().onClick
			.AddListener(() => OnCampaignSelected(camp));
	}

	private void OnCampaignSelected(CampaignProperties camp) {
		switch (camp.CampaignStatus) {
			case AvailableType.NotAvailable:
				lockedCampaignSelected.Invoke(camp);
				break;
			case AvailableType.Available:
				availableCampaignSelected.Invoke(camp);
				break;
			case AvailableType.Passed:
				Debug.Log($"OnCampaignSelected {camp.Title}: AvailableType.Passed");
				break;
		}
	}
}


[System.Serializable]
public class CampaignSelectedEvent : UnityEvent<CampaignProperties> { }