﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Utils;

public class Player : MonoBehaviour {
//	private const string PLAYER_CHILD_NAME = "PlayerTank";
//	private const string TARGET_CHILD_NAME = "PlayerMoveTarget";
//	
//	private NavMeshAgent playerNavMeshAgent;
//	private Transform player;
//	private Rigidbody playerRigidBody;
//
//	public Vector3 Velocity => playerRigidBody.velocity;
//	public Vector3 Position => player.position;
//	
//
//	private void Awake() {
//		player = transform.GetComponentInChildren<Transform>(PLAYER_CHILD_NAME);
//		playerNavMeshAgent = transform.GetComponentInChildren<NavMeshAgent>(PLAYER_CHILD_NAME);
//		playerRigidBody = transform.GetComponentInChildren<Rigidbody>(PLAYER_CHILD_NAME);
//	}
    
    public static GameObject Find() {
        var player = GameObject.FindGameObjectWithTag("player");
		
        if (player == null)
            Debug.LogError("Cannon find player");

        return player;
    }
}
