﻿using System.Collections;
using UniRx.Triggers;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.AI;

public class AiSystem : MonoBehaviour {
    public Transform attackRegion;
    public LayerMask attackMask;
    
    public Transform positionsContainer;

    [SerializeField] private int positionIndex = 0;
    public float positionChangeTime = 2f;

    private CannonSystem cannonSystem;
    private NavMeshAgent navMeshAgent;

    private WaitForSeconds waitYield;
    private Transform[] positions;

    private void Awake() {
        cannonSystem = GetComponentInChildren<CannonSystem>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        
        waitYield = new WaitForSeconds(positionChangeTime);
        
        positions = new Transform[positionsContainer.childCount];
        for (int i = 0; i < positionsContainer.childCount; i++) {
            positions[i] = positionsContainer.GetChild(i);
        }
    }

    private void OnEnable() {
        attackRegion.OnTriggerStayAsObservable().Subscribe(FireRegionTrigger);
        StartCoroutine(MoveCoroutine());
    }

    private void FireRegionTrigger(Collider collider) {
        var gm = collider.gameObject;
        var val = (1 << gm.layer) & attackMask.value;
        
        if (val > 0) {
            cannonSystem.Shot(gm.transform.position);
        }
    }
    
    private IEnumerator MoveCoroutine() {
        navMeshAgent.destination = positions[0].position;
        
        while (true) {
            while (navMeshAgent.pathStatus != NavMeshPathStatus.PathComplete)
                yield return null;

            yield return waitYield;
            
            // repeat in [0..positions.Length)
            positionIndex = ++positionIndex % positions.Length;
            
            var pos = positions[positionIndex].position;
            navMeshAgent.destination = pos;
//            moveSystem.targetTransform = pos;
        }
        // ReSharper disable once IteratorNeverReturns
    }
}