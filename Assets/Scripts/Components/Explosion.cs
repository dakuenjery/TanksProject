using UnityEngine;
using System.Collections;
using QuickPool;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(ParticleSystem))]
public class Explosion : MonoBehaviour {
	private ParticleSystem particleSystem;
	private AudioSource audio;

	public LayerMask layerMask;

	public float damage;
	public float radius;

	private void Awake() {
		particleSystem = GetComponent<ParticleSystem>();
		audio = GetComponent<AudioSource>();
	}

	public void Play() {
		particleSystem.Play();
		StartCoroutine(DespawnDelay());

		Collide();
	}

	private void Collide() {
		var expPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layerMask);

		for (int i = 0; i < colliders.Length; i++) {
			var targetDamageSystem = colliders[i].GetComponent<DamageSystem>();

#if UNITY_EDITOR
			if (targetDamageSystem == null) {
				Debug.Log("DamageSystem not attached to " + colliders[i].name);
				continue;
			}
#endif

			targetDamageSystem.AddExplosion(expPos, damage, radius);
		}
		
		audio.Play();
	}

	private IEnumerator DespawnDelay() {
		yield return new WaitForSeconds(particleSystem.main.duration);
		this.gameObject.Despawn();
	}

#if UNITY_EDITOR
	private void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, radius);

		Handles.Label(transform.position, string.Format("☢ {0}", damage));
	}
#endif
}
