﻿using System;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.AI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MoveSystem : MonoBehaviour {
    private Rigidbody rigidBody;

    public Vector2 direction;

    public FloatReactiveProperty fuelCount = new FloatReactiveProperty(3f);
    public BoolReactiveProperty unlimitedFuel = new BoolReactiveProperty(false);

    public int maxFuel = 3;

    [SerializeField] private Driver driver;

    float fuelBurnRate = .001f;

    private void Awake() {
        rigidBody = GetComponent<Rigidbody>();
        
        driver.Init(transform, rigidBody);
    }

    private void FixedUpdate() {
        if (fuelCount.Value <= 0)
            return;
            
        var burned = driver.Drive(direction);

        if (fuelCount.Value > maxFuel)
            fuelCount.Value = maxFuel;
        
        if (unlimitedFuel.Value == false)
            fuelCount.Value -= burned * fuelBurnRate;
    }
    

#if UNITY_EDITOR
    private void OnDrawGizmos() {
        DrawArrow.ForGizmo(transform.position, transform.forward * 10, Color.blue, 2f, 20f);
    }
#endif
    
}


[System.Serializable]
struct Driver {
    private Rigidbody rigidBody;
    private Transform transform;
    
    private Vector3 forceVec;
    private Vector3 forcePositionVec;

    public float maxSpeed;
    public float maxForce;

    public float maxTurnSpeed;
    public float maxTurnForce;

    public AnimationCurve forceCurve; // speed -> force
    public AnimationCurve turnCurve; // angular speed -> angular force

    public void Init(Transform transform, Rigidbody rigidBody) {
        this.rigidBody = rigidBody;
        this.transform = transform;
    }

    public float Drive(Vector2 direction) {
        var magn = direction.magnitude;

        // move force
        var mf = forceCurve.Evaluate(rigidBody.velocity.magnitude/maxSpeed) * magn;

        forceVec = transform.forward * maxForce * mf;

        // turn force
        var tf = turnCurve.Evaluate(rigidBody.angularVelocity.magnitude/maxTurnSpeed);
        direction *= -tf * maxTurnForce;
        
        forcePositionVec.x = direction.x;
        forcePositionVec.z = direction.y;
        
        rigidBody.AddForceAtPosition(forceVec, rigidBody.position + forcePositionVec);

        return mf;
    }
}


public class HelpBoxInfoAttribute : System.Attribute {}