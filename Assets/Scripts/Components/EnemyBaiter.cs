﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyBaiter : MonoBehaviour {

	public Vector3 camOffset;
	public float baitOffsetFactor = .2f;

	public List<Transform> triggeredObjs = new List<Transform>(10);

	private void OnTriggerEnter(Collider other) {
		triggeredObjs.Add(other.transform);
		UpdateCameraOffset();
	}

	private void OnTriggerExit(Collider other) {
		triggeredObjs.Remove(other.transform);
		UpdateCameraOffset();
	}

	private void UpdateCameraOffset() {
		camOffset = Vector3.zero;

		if (triggeredObjs.Count > 0) {
			var vecSum = triggeredObjs.Aggregate(Vector3.zero, (acc, tr) => acc + tr.position);
			var center = vecSum / triggeredObjs.Count;

			camOffset = (center - transform.position) * baitOffsetFactor;
		}
	}
	
#if UNITY_EDITOR
	private void OnDrawGizmos() {
		var wireCureSize = new Vector3(3, .2f, 3);
		
		foreach (var obj in triggeredObjs) {
			Gizmos.DrawWireCube(obj.position, wireCureSize);
		}
	}
#endif
}
