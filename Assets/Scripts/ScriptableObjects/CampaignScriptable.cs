﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public enum AvailableType {
    NotAvailable, Available, Passed
}


[System.Serializable]
public struct Cash {
    public int coins;
    public int keys;

    public bool Equals(Cash other) {
        return coins == other.coins && keys == other.keys;
    }

    public override bool Equals(object obj) {
        if (ReferenceEquals(null, obj)) return false;
        return obj is Cash && Equals((Cash) obj);
    }

    public override int GetHashCode() {
        unchecked {
            return (coins * 397) ^ keys;
        }
    }

    public static Cash operator +(Cash left, Cash right) => new Cash() {
        coins = left.coins + right.coins,
        keys = left.keys + right.keys
    };
    
    public static Cash operator -(Cash left, Cash right) => new Cash() {
        coins = left.coins - right.coins,
        keys = left.keys - right.keys
    };

    public static bool operator <(Cash left, Cash right) => left.coins < right.coins && left.keys < right.keys;
    public static bool operator >(Cash left, Cash right) => left.coins > right.coins && left.keys > right.keys;
    public static bool operator <=(Cash left, Cash right) => left.coins <= right.coins && left.keys <= right.keys;
    public static bool operator >=(Cash left, Cash right) => left.coins >= right.coins && left.keys >= right.keys;
}


[CreateAssetMenu(fileName = "CampaignProperties.asset", menuName = "CampaignProperties asset")]
public class CampaignProperties : ScriptableObject {
    public AvailableType status = AvailableType.NotAvailable;

    [SerializeField] private string title;
    [SerializeField] private Cash award;
    [SerializeField] private Cash cost;
    
    [SerializeField] private List<CampaignLevel> levels;

    private int currentLvl = -1;

    public string Title => title;
    public Cash Award => award;
    public Cash Cost => cost;
    public AvailableType CampaignStatus => status;

    public bool GetCurrentLevel(out CampaignLevel level) {
        if (currentLvl != -1 && currentLvl >= 0 && currentLvl < levels.Count) {
            level = levels[currentLvl];
            return true;
        }

        level = null;
        return false;
    }

    public void Restart() {
        levels[0].status = AvailableType.Available;

        for (int i = 1; i < levels.Count; ++i)
            levels[i].status = AvailableType.NotAvailable;
    }

    public void StartLevel(CampaignLevel level) {
        for (int i = 0; i < levels.Count; i++)
            if (levels[i].Name == level.Name)
                StartLevel(i);
    }

    public void StartLevel(int id) {
        Assert.IsTrue(id < levels.Count);
        currentLvl = id;
    }

    public void FinishLevel() {
        levels[currentLvl].status = AvailableType.Passed;
        
        if (++currentLvl < levels.Count)
            levels[currentLvl].status = AvailableType.Available;

        currentLvl = -1;
        
        if (levels.All(x => x.status == AvailableType.Passed))
            status = AvailableType.Passed;
    }

    public List<CampaignLevel> Levels => levels;
}


[CreateAssetMenu(fileName = "CampaignStatus.asset", menuName = "CampaignStatus asset")]
public class CampaignStatus : ScriptableObject {
    [SerializeField] private Cash cash;

    [SerializeField] private int currentCampaign = -1;

    public CampaignProperties[] Campaigns;

    public int Coins => cash.coins;
    public int Keys => cash.keys;

    public bool IsInCampaign => currentCampaign >= 0 && currentCampaign < Campaigns.Length;

    public bool GetCurrentCampaign(out CampaignProperties campaign) {
        if (IsInCampaign) {
            campaign = Campaigns[currentCampaign];
            return true;
        }

        campaign = null;
        return false;
    }

    public bool GetCurrentLevel(out CampaignLevel level) {
        CampaignProperties camp;

        if (GetCurrentCampaign(out camp))
            return camp.GetCurrentLevel(out level);

        level = null;
        return false;
    }

    public void StartCampaign(CampaignProperties campaign) {
        for (int i = 0; i < Campaigns.Length; i++)
            if (Campaigns[i].name == campaign.name) 
                StartCampaign(i);
    }

    public void StartCampaign(int id) {
        Assert.IsTrue(Campaigns.Length > id);
        currentCampaign = id;
        Campaigns[id].Restart();
    }

    public void StartLevel(CampaignLevel level) {
        Assert.IsTrue(currentCampaign > -1 && currentCampaign < Campaigns.Length);
        Campaigns[currentCampaign].StartLevel(level);
    }

    public void FinishLevel() {
        Assert.IsTrue(currentCampaign > -1 && currentCampaign < Campaigns.Length);
        Campaigns[currentCampaign].FinishLevel();
    }

    public void EndCampaign() {
        if (!IsInCampaign)
            return;
        
        cash += Campaigns[currentCampaign].Award;
        currentCampaign = -1;
    }

    public void AddKeys(int count) {
        cash.keys += count;
    }

    public bool BuyCampaign(int id) {
        var camp = Campaigns[id];

        if (camp.Cost > cash)
            return false;

        cash -= camp.Cost;
        camp.status = AvailableType.Available;
        return true;
    }

    private void OnEnable() {
        UpdateCampaigns();
    }

    public void UpdateCampaigns() {
        Campaigns = Resources.FindObjectsOfTypeAll<CampaignProperties>()
            .OrderBy(x => x.name).ToArray();
    }

    public void ShowCampaigns() {
        Debug.Log("ShowCampaigns");
        foreach (var camp in Campaigns) {
            Debug.Log(camp);
        }
    }
}