﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class HpUIComponent : MonoBehaviour {

	[SerializeField] private LinearChildAnimator childAnimator;

	private void Awake() {
		childAnimator.parent = transform;
		
		var player = Player.Find();
		
		player?.GetComponent<DamageSystem>().hp
			.StartWith(0)
			.Pairwise()
			.Subscribe(x => childAnimator.Update(x.Current, x.Previous));
	}
}
